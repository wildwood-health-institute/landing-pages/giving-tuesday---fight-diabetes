import App from "./App.svelte";

const app = new App({
	target: document.querySelector('#cover'),
});

document.querySelector(".button.donate").addEventListener('click', e => {
	e.preventDefault()

	const popup = document.getElementById("popup")
	const close = () => {
		document.body.style.overflow = "";
		popup.classList.remove("show")
	}

	document.body.style.overflow = "hidden";
	popup.classList.add("show")

	document.querySelector(".close").addEventListener("click", close, {
		once: true
	})
	document.querySelector(".overlay").addEventListener("click", close, {
		once: true
	})
	return false
})

export default app;